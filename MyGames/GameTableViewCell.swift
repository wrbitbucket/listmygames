//
//  GameTableViewCell.swift
//  MyGames
//
//  Created by Wagner Rodrigues on 02/02/2018.
//  Copyright © 2018 Wagner Rodrigues. All rights reserved.
//

import UIKit

class GameTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var ivCove: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbConsole: UILabel!
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepare(with game: Game) {
        lbTitle.text = game.title ?? ""
        lbConsole.text = game.console?.name ?? ""
        if let image = game.cover as? UIImage {
            ivCove.image = image
        } else {
            ivCove.image = UIImage(named: "noCover")
        }
    }

}
