//
//  ViewController+CoreDate.swift
//  MyGames
//
//  Created by Wagner Rodrigues on 02/02/2018.
//  Copyright © 2018 Wagner Rodrigues. All rights reserved.
//

import UIKit
import CoreData


extension UIViewController {
    
    var context: NSManagedObjectContext {
        let appDelegatge = UIApplication.shared.delegate as! AppDelegate
        return appDelegatge.persistentContainer.viewContext
    }
}
